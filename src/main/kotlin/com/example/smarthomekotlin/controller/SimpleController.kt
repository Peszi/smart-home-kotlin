package com.example.smarthomekotlin.controller

import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController

@RestController
internal class SimpleController {

    @GetMapping("/hello")
    fun getHello(@RequestParam name: String): String {
        return "hello $name"
    }

}