package com.example.smarthomekotlin

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class ShKotlinApplication

fun main(args: Array<String>) {
    runApplication<ShKotlinApplication>(*args)
}
