package com.example.smarthomekotlin.controller

import org.junit.Test

import org.junit.Assert.*
import org.junit.Ignore
import org.junit.runner.RunWith
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.junit4.SpringRunner
import java.net.InetAddress
import java.net.Socket
import javax.validation.constraints.AssertTrue

// Request: 'GET /hello?name=John HTTP/1.1\nHost: localhost:8080\n\n'
class SimpleControllerTest {

    companion object {
        const val SERVER_IP = "localhost"
        const val SERVER_PORT = 8080
        const val RESPONSE_BUFFER_SIZE = 64 // accept any positive value

        const val TEST_VALUE = "John"
    }

    @Ignore
    @Test
    fun getHello() {
        // prepare connection (TCP protocol)
        val socket = Socket(InetAddress.getByName(SERVER_IP), SERVER_PORT)
        val outStream = socket.getOutputStream()
        val isStream = socket.getInputStream()

        // write request payload
        outStream.write(getRequestPayload(TEST_VALUE))
        outStream.flush()

        // read all response data
        var response = String()
        val dataBuffer = ByteArray(RESPONSE_BUFFER_SIZE)
        while (true) {
            val dataLen = isStream.read(dataBuffer)
            response += String(dataBuffer.copyOf(dataLen))
            if (dataLen < RESPONSE_BUFFER_SIZE) break
        }

        // close connection
        isStream.close()
        outStream.close()
        socket.close()

        // show response data
        println(response)
        assertTrue(response.endsWith("hello $TEST_VALUE"))
    }

    private fun getRequestPayload(name: String) = ( // MIN PAYLOAD:
            "GET /hello?name=$name HTTP/1.1\n" +    // line 1: method + _ + path + _ + HTTP/1.1 + \n (\n - new line char)
                    "Host: localhost:8080\n\n"              // line 2: Host: + _ + host + \n + \n (host - any value)
            ).toByteArray(Charsets.UTF_8)
}